//
// Created by Xuan on 2020/3/19.
//
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>

typedef struct {
    unsigned int length;
    char **char_list;
} list;

list *init_list() {
    list *list1 = (list *) malloc(sizeof(list));
    memset(list1, 0, sizeof(list));
    list1->length = 0;
    return list1;
};

int append(list *list, char *content) {
    long long str_len = strlen(content);
    list->length++;
    list->char_list = (char **) realloc(list->char_list, sizeof(char *) * list->length);
    if (list->char_list == NULL) {
        printf("Don't have enough memory");
        return 1;
    }
    list->char_list[list->length - 1] = (char *) malloc((size_t) str_len + 1);
    memcpy(list->char_list[list->length - 1], content, str_len * sizeof(char));
    list->char_list[list->length - 1][str_len] = '\0';
    return list->length;
}

int insert(list *list, int index, char *content) {
    if (index >= list->length && index != -1) {
        printf("Index is invalid\n");
        return -1;
    }  //边界检查
    long long str_len = strlen(content);
    list->length++;
    list->char_list = (char **) realloc(list->char_list, sizeof(char *) * list->length);
    if (list->char_list == NULL) {
        printf("Memory allocate failed. Don't have enough memory\n");
    }
    if (index == -1) {
        list->char_list[list->length - 1] = malloc(strlen(content) + 1);
        memcpy(list->char_list[list->length - 1], content, strlen(content));
        list->char_list[list->length - 1][strlen(content)] = '\0';
        return 0;
    }
    size_t last_loc_size = strlen(list->char_list[list->length - 2]);
    list->char_list[list->length - 1] = malloc(last_loc_size + 1);//新增一位
    //大小为最后一位大小
    memcpy(list->char_list[list->length - 1], list->char_list[list->length - 2], last_loc_size);
    list->char_list[list->length - 1][last_loc_size] = '\0';
    //最后一位已处理完

    for (int i = list->length - 3; i >= index - 1; i = i - 1) {
        char *temp = list->char_list[i];
        size_t cur_block_size = strlen(temp);
        list->char_list[i + 1] = (char *) realloc(list->char_list[i + 1], cur_block_size + 1);
        memset(list->char_list[i + 1], 0, cur_block_size + 1);
        memcpy(list->char_list[i + 1], temp, cur_block_size);
        list->char_list[i + 1][cur_block_size] = '\0';
    } //循环处理

    list->char_list[index - 1] = (char *) realloc(list->char_list[index - 1], (size_t) str_len + 1);
    memcpy(list->char_list[index - 1], content, (size_t) str_len);
    list->char_list[index - 1][str_len] = '\0'; //插入数据
    return 0;
}

int length(list *list) {
    return list->length;
}

char *get(list *list, int index) {
    if (index >= list->length) {
        printf("index is invalid");
        return "";
    }
    char *cont = list->char_list[index - 0];
    return cont;
}

char *pop(list *list) {
    int index = list->length;
    char *last = malloc(strlen(list->char_list[index - 1]) + 1);
    memcpy(last, list->char_list[index - 1], strlen(list->char_list[index - 1]));
    last[strlen(list->char_list[index - 1])] = '\0';
    free(list->char_list[index - 1]);
    list->char_list[index - 1] = NULL;
    list->length -= 1;
    return last;
}


void pprint(list *list) {
    int len = list->length;
    for (int i = 0; i < len; ++i) {
        printf("%s\n", list->char_list[i]);
    }
}


int main() {
    list *mylist = init_list();
    append(mylist, "hello world");
    append(mylist, "hello xuan");
    append(mylist, "hello c");
    append(mylist, "hello python language");
    append(mylist, "hello golang");
    insert(mylist, 2, "hello javascript");
    insert(mylist, 3, "love c++");
    insert(mylist, 1, "pop func");
    insert(mylist, -1, "all ok");
    pprint(mylist);
    printf("%d\n", length(mylist));
    printf("%s\n", pop(mylist));
    printf("%d\n", length(mylist));
    return 0;
}